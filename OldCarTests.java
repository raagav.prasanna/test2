import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class OldCarTests {
    @Test
    public void testConstructor() {
        OldCar bmw = new OldCar(10);
        assertEquals(10,bmw.getSpeed());
        assertEquals(0, bmw.getSpeedCount());
    }
    @Test
    public void testSpeedUp() {
        OldCar nissan = new OldCar(20);
        nissan.speedUp(10);
        assertEquals(30, nissan.getSpeed());
        assertEquals(1, nissan.getSpeedCount());
        nissan.speedUp(20);
        assertEquals(50, nissan.getSpeed());
        assertEquals(2, nissan.getSpeedCount());
        nissan.speedUp(50);
        assertEquals(50, nissan.getSpeed());
        assertEquals(3, nissan.getSpeedCount());
    }
    @Test
    public void testStop() {
        OldCar honda = new OldCar(20);
        honda.speedUp(20);
        honda.stop();
        assertEquals(0, honda.getSpeed());
        assertEquals(0, honda.getSpeedCount());
    }
    @Test
    public void testGetSpeedCount() {
        OldCar toyota = new OldCar(30);
        toyota.speedUp(5);
        toyota.speedUp(5);
        toyota.speedUp(5);
        toyota.speedUp(5);
        assertEquals(4,toyota.getSpeedCount());
        toyota.stop();
        assertEquals(0, toyota.getSpeedCount());
    }
    @Test
    public void testGetSpeed() {
        OldCar ford = new OldCar(0);
        ford.speedUp(-5);
        assertEquals(-5, ford.getSpeed());
        ford.stop();
        assertEquals(0, ford.getSpeed());
    }
    @Test
    public void testSetSpeed() {
        OldCar dodge = new OldCar(80);
        dodge.setSpeed(90);
        assertEquals(90, dodge.getSpeed());
    }
}
