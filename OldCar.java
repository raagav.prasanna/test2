public class OldCar extends Car{
    private int speedCount;
    public OldCar(int speed) {
        super(speed);
        this.speedCount = 0;
    }
    public void speedUp(int speed) {
        this.speedCount++;
        if(this.speedCount % 3 == 0) {
            System.out.println("Car has stalled, speed remains at: " +this.getSpeed());
        }
        else {
            this.setSpeed(speed + this.getSpeed());
            System.out.println("Speed increased to: " +this.getSpeed());
        }
    }
    public void stop() {
        this.setSpeed(0);
        this.speedCount = 0;
    }
    public int getSpeedCount() {
        return this.speedCount;
    }
}
